package br.com.mastertech.cep.controllers;

import br.com.mastertech.cep.clients.Endereco;
import br.com.mastertech.cep.services.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {

    @Autowired
    private CepService cepService;

    @GetMapping("/cep/{cep}")
    public Endereco consultaCep(@PathVariable String cep){
        return cepService.consultarCep(cep);
    }
}
