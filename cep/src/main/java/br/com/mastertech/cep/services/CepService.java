package br.com.mastertech.cep.services;

import br.com.mastertech.cep.clients.Endereco;
import br.com.mastertech.cep.clients.CepClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepClient cepClient;

    public Endereco consultarCep(String cep){
        return cepClient.getEndereco(cep);
    }
}
