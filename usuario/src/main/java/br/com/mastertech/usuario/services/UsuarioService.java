package br.com.mastertech.usuario.services;

import br.com.mastertech.usuario.clients.CepClient;
import br.com.mastertech.usuario.clients.Endereco;
import br.com.mastertech.usuario.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private CepClient cepClient;

    public Usuario cadastrarUsuario(String nome, String cep){
        Endereco endereco = cepClient.getEndereco(cep);
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setCep(endereco.getCep());
        usuario.setLogradouro(endereco.getLogradouro());
        usuario.setBairro(endereco.getBairro());

        return usuario;
    }
}
