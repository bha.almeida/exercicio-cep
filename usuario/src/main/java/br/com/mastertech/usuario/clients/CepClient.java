package br.com.mastertech.usuario.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep")
public interface CepClient {

    @GetMapping("/cep/{cep}")
    Endereco getEndereco(@PathVariable(name = "cep") String cep);
}
