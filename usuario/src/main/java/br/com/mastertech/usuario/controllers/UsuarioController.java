package br.com.mastertech.usuario.controllers;

import br.com.mastertech.usuario.DTOs.UsuarioDTO;
import br.com.mastertech.usuario.models.Usuario;
import br.com.mastertech.usuario.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public Usuario cadastrarUsuario(@RequestBody UsuarioDTO usuarioDTO){
        return usuarioService.cadastrarUsuario(usuarioDTO.getNome(), usuarioDTO.getCep());
    }
}
